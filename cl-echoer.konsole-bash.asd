(asdf:defsystem #:cl-echoer.konsole-bash
  :version "1.0"
  :license "MIT"
  :author "Akasha Peppermint"
  :serial t 
  :depends-on (#:alexandria #:cl-echoer)
  :components ((:file "package")
               (:file "konsole-bash-echoer")))
