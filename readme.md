﻿# What

Streams data from [cl-echoer](https://gitlab.com/akashadutchie/cl-echoer) to a split screen terminal

Supports 8 screens for one instance.

## Usage
```
(ql:quickload '(:cl-echoer.konsole-bash))
(defvar *kech* (cl-echoer.konsole-bash:make-konsole-echoer))

;; Send messages to konsole screens
(cl-echoer:message
 (cl-echoer.konsole-bash:konsole-echoer *kech*)
 :host-1 "Hello World x1")
 
(cl-echoer:message
 (cl-echoer.konsole-bash:konsole-echoer *kech*)
 :host-2 "Foo")
 
;; Change the message on whichever screen tracking host-1
(cl-echoer:message
 (cl-echoer.konsole-bash:konsole-echoer *kech*)
 :host-1 "Hello World x2")
 
;; Move message to a different screen
(cl-echoer:highlight (cl-echoer.konsole-bash:konsole-echoer *kech*) :host-2 5)

;; Re-launch konsole in case closed
(cl-echoer.konsole-bash:launch *kech*)
```
