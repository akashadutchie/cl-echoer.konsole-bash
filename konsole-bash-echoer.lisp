(in-package :cl-echoer.konsole-bash)

(defstruct konsole
  dir
  pid
  auto-track
  echoer) 
;; can use struct inheritance instead but whatever

(defun pidfile-filepath (konsole)
  (merge-pathnames (format nil ".~A.echoer-pidfile.tmp" (file-differentiator-string (konsole-echoer konsole)))
                   (konsole-dir konsole)))

(defun dirlock-filepath (konsole lock#)
  (merge-pathnames (format nil ".~A~A.echo-lock"
                           lock# 
                           (file-differentiator-string (konsole-echoer konsole)) lock#)
                   (konsole-dir konsole)))

(defun make-konsole-echoer
    (&optional
       (file-differentiator-string "")
       (konsole-directory (merge-pathnames #P"/tmp/echoer/" (package-name *package*)))
       (echoer-directory konsole-directory)
     &key (launch-konsole t) (autotrack t) (watch-cap 8))

  (ensure-directories-exist konsole-directory)
  (let ((res (make-konsole
              :echoer (make-echoer file-differentiator-string echoer-directory watch-cap)
              :dir konsole-directory
              :auto-track autotrack)))
    (if launch-konsole
        (launch res))
    res))

(defun launch (konsole)
  (setf
   (konsole-pid konsole)
   (let* ((konsole-pidfile (open (pidfile-filepath konsole) :direction :input :if-does-not-exist :create))
          (res-pid (let* ((pidstring (read-line konsole-pidfile nil ""))
                          (pid (parse-integer pidstring :junk-allowed t))
                          (pidof-konsole-raw (uiop:run-program "pidof konsole" :output :string :ignore-error-status t)))
                     (close konsole-pidfile) 
                     (if (and (< 1 (length pidof-konsole-raw))
                              (member pidstring (str:split " " (subseq pidof-konsole-raw 0 (- (length pidof-konsole-raw) 1)))
                                      :test #'equalp))
                         pid
                         (let ((process (uiop:launch-program
                                         `("konsole" "--hide-menubar" "--layout"
                                                     ,(namestring (asdf:system-relative-pathname :cl-echoer.konsole-bash "konsole-8-pane.json"))))))
                           (setf pid (uiop:process-info-pid process))

                           ;; save the new pid
                           (write-string-into-file
                            (write-to-string pid)
                            (pidfile-filepath konsole)
                            :if-exists :supersede
                            :if-does-not-exist :create)
                           
                           ;; wait for bash stuff to load
                           ;; Could make this smarter if the number of panes is known.
                           ;; Check for amount of bash processes with pidof
                           ;;
                           ;; even still, would like to get rid of polling altogether.
                           ;; (format konsole-pidfile "~A" pid) ; y no wrok
                           
                           (sleep 0.5)
                           (loop
                             for i from 1
                             for j from 0
                             for code = (nth-value 2
                                                   (uiop:run-program `("qdbus" ,(format nil "org.kde.konsole-~A" pid)
                                                                               ,(format nil "/Sessions/~A" i))
                                                                     :ignore-error-status t))
                             ;; code should return 2 when no more sessions.
                             if (not (eq code 0))
                               do
                                  (return)
                             else do
                               ;; Set initial title
                               (uiop:run-program `("qdbus" ,(format nil "org.kde.konsole-~A" pid)
                                                           ,(format nil "/Sessions/~A" i)
                                                           "setTitle"
                                                           "1"
                                                           ,(format nil "Tracker #~A" i)))
                               ;; This trick works for konsole 21.08.1 though it does trigger an alert.
                               ;; way safer than messing with the bashrc file anyway.
                               (uiop:run-program
                                `("qdbus" ,(format nil "org.kde.konsole-~A" pid)
                                          ,(format nil "/Sessions/~A" i)
                                          "runCommand"
                                          ,(format nil "~
clear && while [ -p \"~A\" ] ; do ~
(while [ -p \"~A\" ]; do echo -ne \"\\033]30;Tracker #~A\\007\"; var=$( cat ~A ); clear ; echo \"$var\"; done) & ~
(while [ -p \"~A\" ]; do read -n 1 -s -r key; echo \"~A\" > \"~A\"; echo \"$key\" > \"~A\"; done) ; done ;"
                                                   ;; Causes freeze on recycled instances and doesnt do what i want
                                                   ;; [[ `pidof sbcl` == \"~A\" ]] &&  
                                                   ;; (sb-posix:getpid)
                                                   (namestring (track-filepath (konsole-echoer konsole) j)) 
                                                   ;; (wait ~A && kill ~A) &~ 
                                                   ;; (sb-posix:getpid)
                                                   ;; pid
                                                   (namestring (track-filepath (konsole-echoer konsole) j))
                                                   j
                                                   (namestring (track-filepath (konsole-echoer konsole) j))
                                                   (namestring (track-filepath (konsole-echoer konsole) j))
                                                   j
                                                   (namestring (writeback-filepath (konsole-echoer konsole)))
                                                   (namestring (writeback-filepath (konsole-echoer konsole)))
                                                   ;; && wait ~A && kill ~A
                                                   ;; (sb-posix:getpid)
                                                   ;; pid
                                                   )))
                             ))))))
     
     res-pid)))
