(defpackage #:cl-echoer.konsole-bash
  (:use #:cl #:alexandria #:cl-echoer)
  (:nicknames #:echo)
  (:export #:konsole
           #:konsole-pid
           #:konsole-auto-track
           #:konsole-echoer
           #:make-konsole-echoer
           #:pidfile-filepath 
           #:dirlock-filepath
           #:launch

           ;; From cl-echoer
           #:echoer
           #:make-echoer
           #:message 
           #:highlight 
           #:update
           
           #:file-differentiator-string
           #:dir
           #:auto-track
           #:tracked
           #:host->num   
           #:hostnum->process
           
           #:message-filepath 
           #:track-filepath   
           #:writeback-filepath 
           #:create-message-file 
           #:autotrack
           
           #:%highlight  
           #:%link-tracker 
           #:%spawn-sink 
           #:%message
           #:%try-initialise
           #:message-copy-filepath 
           #:track-copy-filepath))
